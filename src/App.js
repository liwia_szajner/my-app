import React, { Component } from 'react';
import chef from './chef.png';
import potato from './potato.png';
import tomato from './tomato.svg';
import './App.css';
import { Button,Row, Col} from 'antd';
import Image from 'react-bootstrap/Image';
import Lokal from './components/Lokal/Lokal.js';


let counter = 0
class App extends Component {
  state={
    forms:[]
  };
  addForms=()=>{
    let lista1 = this.state.forms
    lista1.push(counter)
    this.setState({forms:lista1})
    counter+=1
    console.log(this.state.forms)
  }
  render() {
    return (
        <div>
          <div id = "pot">
            <img src = {potato} className="vege" />
          </div>
          <div id = "tom">
            <img src = {tomato}  className="vege"  />
          </div>
          
          <Row>
          <h1 className="Main-title">
                Obiady GIAP
          </h1>
          </Row>
          
          <Row>
            <Col><Image src={chef} className="Chef" /></Col>
          </Row>
          <Row>
            <Col offset = {10}>
              <Button className="btn_add" onClick={(e) => {
                            this.addForms(e)
                          }}
                        >
                        Dodaj nowy lokal
              </Button>
            </Col>
          </Row>
          <Row>
          {
            this.state.forms.map((form,index)=>{
              return(
                <div key={(index)}>
                    <Lokal/>
                </div>
              )
            })
          }
          </Row>     
       </div>
    );
  }
}
export default App;
