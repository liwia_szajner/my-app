import React from 'react';
import {
    Form, Input, Icon, Button,Col, Row
  } from 'antd';
import './form.css';
const { TextArea } = Input;

let id = 0;

class DynamicFieldSet extends React.Component {
remove = (k) => {
    const { form } = this.props;
    const keys = form.getFieldValue('keys');

    if (keys.length === 1) {
    return;
    }
    form.setFieldsValue({
    keys: keys.filter(key => key !== k),
    });
}

add = () => {
    const { form } = this.props;
    const keys = form.getFieldValue('keys');
    const nextKeys = keys.concat(id++);
    form.setFieldsValue({
    keys: nextKeys,
    });
}

handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
    if (!err) {
        console.log('Received values of form: ', values);
    }
    });
}

render() {
    const { getFieldDecorator, getFieldValue } = this.props.form;
    const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 4 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 20 },
    },
    };
    const formItemLayoutWithOutLabel = {
    wrapperCol: {
        xs: { span: 24, offset: 0 },
        sm: { span: 20, offset: 4 },
    },
    };
    getFieldDecorator('keys', { initialValue: [] });
    const keys = getFieldValue('keys');
    const formItems = keys.map((k, index) => (
    <Form.Item>
        <Row>
            <Col span={20} offset = {4}>
                {getFieldDecorator(`names[${k}]`, {
                validateTrigger: ['onChange', 'onBlur'],
                rules: [{
                    required: true,
                    whitespace: true,
                    message: "Wpisz zamówienie lub usuń pole !",
                }],
                })(
                <TextArea rows={2} placeholder="Imię Nazwisko Zamówienie" style={{ width: '90%', marginRight: 8 }} />
                )}
                {keys.length > 1 ? (
                <Icon
                    className="dynamic-delete-button"
                    type="minus-circle-o"
                    disabled={keys.length === 1}
                    onClick={() => this.remove(k)}
                />
                ) : null}
            </Col>
        </Row>
    </Form.Item>
    ));
    return (
    <Form onSubmit={this.handleSubmit}>
        {formItems}
        <Form.Item {...formItemLayoutWithOutLabel}>
        <Row>
            <Col span={12} offset = {6} >
                <Button type="dashed" onClick={this.add} style={{ width: '80%' }}>
                    <Icon type="plus" /> Dodaj zamówienie
                </Button>
            </Col>
        </Row>
        </Form.Item>
        <Form.Item {...formItemLayoutWithOutLabel}>
    
        </Form.Item>
    </Form>
    );
}
}

export default DynamicFieldSet;
