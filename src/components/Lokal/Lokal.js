import React from 'react';
import { Form,Input,Row, Col} from 'antd';
import './lokal.css';
import DynamicFieldSet from '../DynamicFieldSet/DynamicFieldSet.js'

const WrappedDynamicFieldSet = Form.create({ name: 'dynamic_form_item' })(DynamicFieldSet);
const { TextArea } = Input;
const lokal = () =>{  
    return(
        <div className ="form">
                <Row>
                    <Col span={4}>
                        <label>Lokal</label>
                    </Col>
                    <Col span={19}> 
                        <Input placeholder="Nazwa lokalu" className="lineedit"/>
                    </Col>
                </Row>
                <Row>
                    <Col span={4}>
                        <label>Osoba zamawiająca</label>
                    </Col>
                    <Col span={19}> 
                        <Input placeholder="Imię i nazwisko" className="lineedit" />
                    </Col>
                </Row>
                <Row>
                    <Col span={4}>
                        <label>Nr konta</label>
                    </Col>
                    <Col span={19}> 
                        <Input placeholder="Numer konta" className="lineedit" />
                    </Col>
                </Row>
                <Row>
                <Col span={12} offset = {12} >
                        <label>Lista zamówień</label>
                    </Col> 
                </Row>
                <Row>
                    <WrappedDynamicFieldSet />
                </Row>       
        </div>
    )
    
}

export default lokal;


